class nodejspack {
  package { ['nodejs', 'npm']:
    ensure => present,
    require => Exec['apt-get update'],
  }

  package { 'pm2':
    ensure   => 'present',
    provider => 'npm',
    require => Package['npm'],
  }
}