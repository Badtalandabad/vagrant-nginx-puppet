class nginx {
  package { 'nginx':
    ensure => 'present',
    require => Exec['apt-get update'],
  }

  # Hosts
  file { 'nginx-default-host':
    path => '/etc/nginx/sites-available/default',
    ensure => file,
    replace => true,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/default',
  }
  file { 'nginx-banjo-host':
    path => '/etc/nginx/sites-available/banjo.local',
    ensure => file,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/banjo.local',
  }
  file { 'nginx-musicconf-host':
    path => '/etc/nginx/sites-available/musicconf.local',
    ensure => file,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/musicconf.local',
  }

  # Symlink vhosts in sites-enabled to enable it
  file { 'nginx-banjo-host-link':
    path => '/etc/nginx/sites-enabled/banjo.local',
    target => '/etc/nginx/sites-available/banjo.local',
    ensure => link,
    #notify => Service['nginx'],
    require => File['nginx-banjo-host'],
  }
  file { 'nginx-musicconf-host-link':
    path => '/etc/nginx/sites-enabled/musicconf.local',
    target => '/etc/nginx/sites-available/musicconf.local',
    ensure => link,
    #notify => Service['nginx'],
    require => File['nginx-musicconf-host'],
  }

  service { 'nginx':
    ensure => running,
    require => Package['nginx'],
  }
}