class php {
  $enhancers = [ 
    'php7.2',
    'php7.2-cli',
    'php7.2-curl',
    'php7.2-dev',
    'php7.2-gmp',
    'php7.2-gd',
    'php7.2-intl',
    'php7.2-json',
    'php7.2-mbstring',
    'php7.2-mysql',
    'php7.2-pgsql',
    'php7.2-readline',
    'php7.2-soap',
    'php7.2-sqlite3',
    'php7.2-xml',
    'php7.2-zip',

    'php7.2-fpm',
  ]
  package { $enhancers:
    ensure  => 'installed',
    require => Exec['apt-get update'],
  }

  # Make sure php7.2-fpm is running
  service { 'php7.2-fpm':
    ensure => running,
    require => Package['php7.2'],
  }

  package { 'php7.2-xdebug':
    ensure  => 'latest',
    require => Package['php7.2'],
  }

  augeas { "php.ini":
    notify  => Service[nginx],
    require => [Package["php7.2"], Package["augeas-tools"]],
    context => "/files/etc/php/7.2/fpm/php.ini",
    #lens => 'Puppet.lns',
    #incl => "/files/etc/php/7.2/fpm/php.ini",
    changes => [
      "set PHP/post_max_size 100M",
      "set PHP/upload_max_filesize 100M",
      "set Date/date.timezone Europe/Samara",
      "set xdebug/zend_extension /usr/lib/php/20170718/xdebug.so",
      "set xdebug/xdebug.remote_enable 1",
      "set xdebug/xdebug.remote_connect_back 1",
      "set xdebug/xdebug.var_display_max_depth -1",
      "set xdebug/xdebug.var_display_max_children -1",
      "set xdebug/xdebug.var_display_max_data -1",
    ];
  }

  package { "curl":
    ensure => installed,
  }
 
  exec { 'install composer':
    command => '/usr/bin/curl -sS https://getcomposer.org/installer | php && sudo mv composer.phar /usr/local/bin/composer',
    require => [Package['curl'], Package['php7.2']],
    cwd    => '/vagrant',
    creates => "/usr/local/bin/composer",
    environment => ["HOME=/home/vagrant"],
  }
}